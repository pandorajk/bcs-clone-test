# PRODUCT LANDING PAGE

# Stack:
HTML/CSS project that creates the landing page for an ecommerce beer company. 


# Functionality: 
Includes functioning nav bar, link tags, embedded google maps and is responive to a smaller screen width.


# Deployment:
See it at:
  https://pandorajk.gitlab.io/bcs_cloning-project_1/#beer


# Set up and Installation: 
-git clone https://gitlab.com/Pandorajk/bcs_cloning-project_1
-open index.html page to see in browser. 


# Commit history: 
1. Initial commit - (02/09/2019)
2. nav bar, splash, headers, googlemaps & text added - ??if they are optimally responsive to the browers page/viewport size
3. splash background image now correct and button added
4. how to make the images in gallery on a grid that responds to viewport size
5. almost finished cp01 - needs finishing touches
6. final


# Contact:
Pandora Jane Knocker - pjknocker@yahoo.com